const tasks = [

    {
        isComplite: false,
        id: 1,
        name: 'Петя',
        age: '25 лет',
        email: 'Pety@amail.ru',
    },
    
    {
        isComplite: false,
        id: 2,
        name: 'Коля',
        age: '17 лет',
        email: 'Koly@amail.ru',
    },

    {
        isComplite: false,
        id: 3,
        name: 'Саша',
        age: '32 года',
        email: 'Sasha@mail.ru',
    },
    
    {
        isComplite: false,
        id: 4,
        name: 'Зина',
        age: '35 лет',
        email: 'Zinaida@mail.ru',
    },

    {
        isComplite: false,
        id: 5,
        name: 'Вася',
        age: '14 лет',
        email: 'Vasiliy@mail.ru',
    }
];

const list = document.querySelector('.container__list');

function createTagTask(task) {
    const name = document.createElement('span');
    name.className='task__name';
    name.innerText = task.name;

    const number = document.createElement('span');
    number.className = 'task__number';
    number.innerText = task.id;

    const age = document.createElement('span');
    age.className = 'task__age';
    age.innerText = task.age;

    const Email = document.createElement('span');
    Email.className = 'task__Email';
    Email.innerText = task.email;
    
    const taskTag = document.createElement('div');
    taskTag.className = 'task';
    taskTag.appendChild(number);
    taskTag.appendChild(name);
    taskTag.appendChild(age);
    taskTag.appendChild(Email);

    return taskTag;
}

tasks.forEach(element => {
    const Users = createTagTask(element);
    list.appendChild(Users);
});